#!/usr/bin/env python

import ast
import flax
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pickle
import sys

import mlp

# Train an observable.

# Don't print annoying CPU warning.
jax.config.update('jax_platform_name', 'cpu')

key = jax.random.PRNGKey(0)

# Read data and renormalization map.
with open(sys.argv[1], 'rb') as f:
    history = pickle.load(f)
with open(sys.argv[2], 'rb') as f:
    dr_model, dr_params = pickle.load(f)

# Available samples
N = len(history)

# The dimensionality of the big space
D = len(history[0][1])

# The target dimensionality
D_dr = dr_model.features[-1]

obs_ikey, key = jax.random.split(key)
obs_model = mlp.MLP([D_dr,1])
obs_params = obs_model.init(obs_ikey, history[0][1])


@jax.jit
def loss(p):
    """
    Loss function for training the observables.
    """
    return 0.

loss_grad = jax.jit(jax.value_and_grad(loss))

opt = optax.adam(1e-2)
opt_state = opt.init(obs_params)
try:
    while True:
        loss_val, grad = loss_grad(obs_params)
        print(f'Current loss: {loss_val}')
        updates, opt_state = opt.update(grad, opt_state)
        obs_params = optax.apply_updates(obs_params, updates)
except KeyboardInterrupt:
    print()
    print(f'Writing, with loss {loss(obs_params)}')

with open(sys.argv[3], 'wb') as f:
    pickle.dump((obs_model, obs_params), f)
