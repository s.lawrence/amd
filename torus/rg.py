#!/usr/bin/env python

# Apply RG to extract effective equations of motion.

import ast
import flax
import jax
import jax.numpy as jnp
import numpy as np
import optax
import pickle
import sys

import mlp

# Don't print annoying CPU warning.
jax.config.update('jax_platform_name', 'cpu')

key = jax.random.PRNGKey(0)

# Read data
with open(sys.argv[1], 'rb') as f:
    history = pickle.load(f)

# TODO track some observable of interest

xs = jnp.array([h[1] for h in history])
vs = jnp.array([h[2] for h in history])

# Available samples
N = len(history)

# The dimensionality of the big space
D = len(history[0][1])

# The target dimensionality
D_dr = 10

print(f'Attempting to map {D} dimensions on to {D_dr}')

# We learn two maps --- a dimensional reduction map and the reduced equations
# of motion.
dr_ikey, eom_ikey, key = jax.random.split(key, 3)
dr_model = mlp.MLP([D,D_dr])
eom_model = mlp.MLP([D_dr,D_dr])
dr_params = dr_model.init(dr_ikey, history[0][1])
eom_params = eom_model.init(eom_ikey, dr_model.apply(dr_params, history[0][1]))
params = (dr_params, eom_params)

def loss_sammon(p_dr, rkey):
    # Number of pairs to use.
    K = N
    xps = dr_model.apply(p_dr, xs)
    i1 = jax.random.choice(rkey, N, shape=(K,))
    i2 = (1+i1+jax.random.choice(rkey, N-1, shape=(K,)))%N
    x, y = xs[i1], xs[i2]
    xp, yp = xps[i1], xps[i2]
    d_true = jnp.linalg.norm(x-y, axis=1)
    d = jnp.linalg.norm(xp-yp, axis=1)

    return jnp.sum((d_true-d)**2/d_true)/jnp.sum(d_true)

def loss_eom(p_dr, p_eom, rkey):
    xps, vps = jax.jvp(lambda x: dr_model.apply(p_dr, x), (xs,), (vs,))
    ws = eom_model.apply(p_eom, xps)
    return jnp.sum((vps-ws)**2)/jnp.sum(vps**2)

@jax.jit
def loss(p, rkey):
    """
    Loss function for training the DR map and EoM.

    The loss is a weighted sum of Sammon's stress (for the dimensional
    reduction) and a simple squared error (for the equations of motion).
    """
    p_dr, p_eom = p
    return loss_sammon(p_dr, rkey) + loss_eom(p_dr, p_eom, rkey)

loss_grad = jax.jit(jax.value_and_grad(loss))

opt = optax.adam(1e-2)
opt_state = opt.init(params)
try:
    while True:
        key, rkey = jax.random.split(key)
        loss_val, grad = loss_grad(params, rkey)
        print(f'Current loss: {loss_val}')
        updates, opt_state = opt.update(grad, opt_state)
        params = optax.apply_updates(params, updates)
except KeyboardInterrupt:
    key, rkey = jax.random.split(key)
    print()
    print(f'Writing, with loss {loss(params, rkey)}')

# Write the renormalization map
with open(sys.argv[2], 'wb') as f:
    pickle.dump((dr_model, dr_params), f)

# Write the renormalized model
with open(sys.argv[2], 'wb') as f:
    pickle.dump((eom_model, eom_params), f)

