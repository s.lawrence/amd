#!/usr/bin/env python

import ast
import flax
import jax
import jax.numpy as jnp
import numpy as np
import pickle
import sys

import mlp

# Read the renormalized model
with open(sys.argv[1], 'rb') as f:
    model, params = pickle.load(f)

