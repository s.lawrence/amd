#!/usr/bin/env python

# A free particle moving along a torus.

import flax
import jax
import jax.numpy as jnp
import numpy as np
import pickle
import sys

import mlp

# Radii
R = jnp.array((10.,1., 0.1, 0.01))
D = len(R)

# Start at the origin
phi = jnp.zeros(D)

# Initialize a random velocity
pi = jnp.array(np.random.normal(size=D))

# Embedding
prngkey = jax.random.PRNGKey(0)
embedmlp = mlp.MLP(features=[20,20])
embedparams = embedmlp.init(prngkey, np.zeros(2*D))

@jax.jit
def embed(phi):
    x = jnp.concatenate([jnp.cos(2*jnp.pi*phi/R), jnp.sin(2*jnp.pi*phi/R)])
    x = embedmlp.apply(embedparams, x)
    return x

@jax.jit
def embed_vel(phi, pi):
    return jax.jvp(embed, (phi,), (pi,))

# Simulate
dt = 0.0001
t = 0
history = []
for _ in range(1000):
    for _ in range(10):
        # Update
        phi += dt * pi
        phi = np.mod(phi, R)
        t += dt

    # Compute position and velocity in the embedding space
    x, v = embed_vel(phi,pi)

    history.append((t,x,v))

with open(sys.argv[1], 'wb') as f:
    pickle.dump(history, f)

