Particle on a torus

`full.py` performs the full simulation.

To visualize, `vis.py`.

Computing renormalized dynamics, `rg.py`.

Efficient (post-renormalization) simulation with `sim.py`.

