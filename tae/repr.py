#!/usr/bin/env python

"""
Reproduction of various results from Wehmeyer&Noe(2017).
"""

import numpy as np
import sys
from typing import Sequence

import flax
import flax.linen as nn
import jax
import jax.numpy as jnp
import optax
# Don't print annoying CPU warning.
jax.config.update('jax_platform_name', 'cpu')
# Debug mode
DEBUG = False
if DEBUG:
    jax.config.update('jax_debug_nans', True)
key = jax.random.PRNGKey(0)

class MLP(nn.Module):
    features: Sequence[int]

    @nn.compact
    def __call__(self, x):
        for i, feat in enumerate(self.features):
            x = nn.Dense(feat)(x)
            if i != len(self.features)-1:
                x = nn.relu(x)
        return x

class TAE(nn.Module):
    features: Sequence[int]
    d: int
    N: int

    @nn.compact
    def __call__(self, x):
        x = MLP(list(self.features)+[self.d])(x)
        x = MLP(list(reversed(self.features))+[self.N])(x)
        return x

def train_model(dat, d, lag, K=10000):
    global key
    N = dat.shape[-1]
    T = dat.shape[0]
    ikey, key = jax.random.split(key)
    model = TAE([200,100],d,N)
    params = model.init(ikey, dat[0,:])
    ts = jnp.array(range(T-lag))

    def loss(p):
        r = 0.
        def loss_(t):
            x = dat[t,:]
            y = dat[t+lag,:]
            yp = model.apply(p,x)
            return jnp.sum(jnp.abs(y-yp)**2)
        return jnp.sum(loss_(ts))

    loss_grad = jax.jit(jax.value_and_grad(loss))

    opt = optax.adam(1e-3)
    opt_state = opt.init(params)

    for k in range(K):
        val, grad = loss_grad(params)
        if k % 200 == 0:
            print(f' {k}   {val/T}')
        updates, opt_state = jax.jit(opt.update)(grad, opt_state)
        params = optax.apply_updates(params, updates)
    return model, params, loss(params)/T

######################
# Two-state toy model

if False:

    # Transition probabilities
    p = np.array([[0.9, 0.1],[0.1,0.9]])

    # Number of time steps
    T = 1000

    # lag
    lag = 5

    # Starting state
    state = 0

    dat = []
    for _ in range(T):
        x = np.random.normal()
        y = 0.3*np.random.normal()
        if state == 1:
            y += 2
        y += np.sqrt(np.abs(x))
        dat.append([x,y])
        # Transition
        state = np.random.choice([0,1], p=p[state,:])
    dat = jnp.array(dat)

    model, params, loss = train_model(dat, 1, lag)
    print(loss)


#######################
# Four-state toy model

if False:
    pass


####################
# Alanine dipeptide

if True:
    pass


#########
# Villin

if False:
    pass

