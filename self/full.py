#!/usr/bin/env python

"""
Full simulation of the equations of motion.
"""

import eom
import jax
import jax.numpy as jnp
import sys

# Don't print annoying CPU warning.
jax.config.update('jax_platform_name', 'cpu')

key = jax.random.PRNGKey(0)

x = eom.init(key)
v = 0*x

T = 5.
t = 0.
dT = 1e-3
while t <= T:
    t += dT
    v += dT * eom.force(x)
    x += dT*v
    print(t, *x)

