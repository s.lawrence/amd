#!/usr/bin/env python

"""
Self-learned, accelerated MD simlation.
"""

import flax
import jax
import jax.numpy as jnp
import optax
import sys

import eom
import models

# Don't print annoying CPU warning.
jax.config.update('jax_platform_name', 'cpu')

key = jax.random.PRNGKey(0)

# Dimension of the reduced system.
D = 3

"""
Three learned models are involved: reduction, motion, and inversion.
"""
red_model = models.MLP([3,D])
eom_model = models.MLP([D,D])
inv_model = models.MLP([D,D])

red_ikey, eom_ikey, inv_ikey, cfg_ikey, key = jax.random.split(key, 5)
x = eom.init(cfg_ikey)
red_params = red_model.init(red_ikey, x)
y = red_model.apply(red_params, x)
eom_params = eom_model.init(eom_ikey, y)
inv_params = inv_model.init(inv_ikey, y)
params = (red_params, eom_params, inv_params)

@jax.jit
def loss(p):
    # TODO
    return 0.

loss_grad = jax.jit(jax.value_and_grad(loss))

opt = optax.adam(1e-2)
opt_state = opt.init(params)
try:
    while True:
        loss_val, grad = loss_grad(params)
        print(f'Current loss: {loss_val}')
        updates, opt_state = opt.update(grad, opt_state)
        params = optax.apply_updates(params, updates)
except KeyboardInterrupt:
    print()

