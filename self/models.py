import flax
import flax.linen as nn
from typing import Sequence

class MLP(nn.Module):
    features: Sequence[int]

    @nn.compact
    def __call__(self, x):
        for i, feat in enumerate(self.features):
            x = nn.Dense(feat)(x)
            if i != len(self.features)-1:
                x = nn.relu(x)
        return x

