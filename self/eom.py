import jax
import jax.numpy as jnp

def force(x):
    return -x

def init(key):
    return jax.random.normal(key, (3,))

