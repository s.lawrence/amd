#include <algorithm>
#include <array>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <random>
#include <set>
#include <utility>
#include <vector>

#define float double

float L;

std::vector<std::pair<int,int>> pairs_torus(const std::vector<std::array<float,3>>, float, float);
float dist(std::array<float,3> p, std::array<float,3> p_);
std::vector<std::array<float,3>> forces(const std::vector<std::array<float,3>> pos);
std::vector<std::array<float,3>> shear_forces(const std::vector<std::array<float,3>> pos);
void measure(std::vector<std::array<float,3>> pos, std::vector<std::array<float,3>> vel);

float potential(float r) {
	return 0.5 * exp(-2*r);
}

float force(float r) {
	// Magnitude of radial force. Positive values are repulsive.
	return 1. * exp(-2*r);
}

int main(int argc, char *argv[]) {
	// Parse arguments.
	if (argc != 8) {
		std::cerr << "usage: " << argv[0];
		std::cerr << " L n T dt thermalization perturbation evolution" << std::endl;
		return 1;
	}

	L = atof(argv[1]);
	float n = atof(argv[2]);
	float T = atof(argv[3]);
	float dt = atof(argv[4]);
	float t_therm = atof(argv[5]);
	float t_pert = atof(argv[6]);
	float t_ev = atof(argv[7]);

	int N = (int)(n*L*L*L);

	// Initialize random positions and velocities.
	std::vector<std::array<float,3>> pos(N), vel(N);
	{
		auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
		if (false) seed = 100;
		std::default_random_engine gen(seed);
		std::uniform_real_distribution<float> pdist(0,L);
		std::normal_distribution<float> vdist(0, sqrt(T));
		for (int i = 0; i < N; i++) {
			for (int d = 0; d < 3; d++) {
				pos[i][d] = pdist(gen);
				vel[i][d] = vdist(gen);
			}
		}
	}

	// Run the time-evolution.
	std::vector<std::array<float,3>> acc(N), acc_(N);
	// Calculate initial accelerations.
	acc = forces(pos);
	for (float t = 0; t < t_therm + t_pert + t_ev; t += dt) {
		// Normalize positions.
		for (int i = 0; i < N; i++) {
			for (int d = 0; d < 3; d++) {
				pos[i][d] = fmod(pos[i][d], L);
			}
		}

		// Update positions.
		for (int i = 0; i < N; i++) {
			for (int d = 0; d < 3; d++) {
				pos[i][d] += vel[i][d]*dt + 0.5 * acc[i][d]*dt*dt;
			}
		}

		// Calculate new accelerations.
		if (t > t_therm && t < t_therm + t_pert) {
			acc_ = shear_forces(pos);
		} else {
			acc_ = forces(pos);
		}

		// Update velocities.
		for (int i = 0; i < N; i++) {
			for (int d = 0; d < 3; d++) {
				vel[i][d] += 0.5 * (acc[i][d] + acc_[i][d]) * dt;
			}
		}

		// Cycle accelerations.
		acc = acc_;

		// Measure
		if (false) {
			// Debug
			for (int i = 0; i < N; i++) {
				std::cout << t << " " << i << " ";
				for (int d = 0; d < 3; d++)
					std::cout << " " << pos[i][d];
				for (int d = 0; d < 3; d++)
					std::cout << " " << vel[i][d];
				std::cout << std::endl;
			}
		} else {
			std::cout << t << " ";
			measure(pos, vel);
		}
	}
	return 0;
}

void measure(std::vector<std::array<float,3>> pos, std::vector<std::array<float,3>> vel) {
	int N = pos.size();
	float shear_amplitude = 0., sound_amplitude = 0., vel1 = 0., vel2 = 0.;
	for (int i = 0; i < N; i++) {
		double ke = (vel[i][0]*vel[i][0] + vel[i][1]*vel[i][1] + vel[i][2]*vel[i][2])/2.;
		shear_amplitude += vel[i][2] / N * cos(2*M_PI*pos[i][0]/L);
		sound_amplitude += cos(2*M_PI*pos[i][0]/L) * ke / N;
		vel1 += vel[i][2]/N;
		vel2 += 2*ke/N;
	}
	for (auto [i,j] : pairs_torus(pos, L, 3)) {
		float r = dist(pos[i], pos[j]);
		float mx = (pos[i][0] + pos[j][0])/2.;
		float v = potential(r);
		sound_amplitude += cos(2*M_PI*mx/L) * v/N;
	}
	std::cout << shear_amplitude << " " << sound_amplitude << " " << vel1 << " " << vel2 << std::endl;
}

std::array<float,3> disp(std::array<float,3> p, std::array<float,3> p_) {
	std::array<float,3> r;
	for (int d = 0; d < 3; d++) {
		r[d] = fmod(p[d] - p_[d],L);
		if (r[d] > 0.5*L) r[d] -= L;
		if (r[d] < -0.5*L) r[d] += L;
	}
	return r;
}

float dist2(std::array<float,3> p, std::array<float,3> p_) {
	auto dsp = disp(p, p_);
	float r = 0.;
	for (int d = 0; d < 3; d++) {
		float disp = dsp[d];
		r += disp*disp;
	}
	return r;
}

float dist(std::array<float,3> p, std::array<float,3> p_) {
	return sqrt(dist2(p,p_));
}

std::vector<std::pair<int,int>>
pairs_torus(const std::vector<std::array<float,3>> pos, float L, float R) {
	// Divide into regions minimally larger than R.
	int M = floor(L/R);
	if (M < 3) M = 3;
	float S = L/M;

	// We have M subdivisions in each direction. Allocate regions.
	std::vector<int> region[M*M*M];

	// Sort the points into regions.
	for (unsigned i = 0; i < pos.size(); i++) {
		int x = int(pos[i][0]/S + M)%M;
		int y = int(pos[i][1]/S + M)%M;
		int z = int(pos[i][2]/S + M)%M;
		int r = z*M*M + y*M + x;
		region[r].push_back(i);
	}
	
	// For each region, do a pairwise search, not just within that region
	// but also with all neighboring regions.
	std::vector<std::pair<int,int>> result;
	for (int x = 0; x < M; x++) for (int y = 0; y < M; y++) for (int z = 0; z < M; z++) {
		int r = z*M*M + y*M + x;
		for (int xp = x-1; xp <= x+1; xp++) for (int yp = y-1; yp <= y+1; yp++) for (int zp = z-1; zp <= z+1; zp++) {
			int rp = ((zp+M)%M)*M*M + ((yp+M)%M)*M + (xp+M)%M;
			for (auto p : region[r]) for (auto pp : region[rp]) {
				if (p == pp) continue;
				if (dist2(pos[p], pos[pp]) < R*R)
					result.push_back(std::make_pair(p,pp));
			}
		}
	}
	return result;
}

std::vector<std::array<float,3>> forces(const std::vector<std::array<float,3>> pos) {
	int N = pos.size();
	std::vector<std::array<float,3>> f(N);
	for (int i = 0; i < N; i++)
		for (int d = 0; d < 3; d++)
			f[i][d] = 0.;

	for (auto [i,j] : pairs_torus(pos, L, 3)) {
		// Calculate the acceleration by considering all nearby particles.
		float r = dist(pos[i], pos[j]);
		float a = force(r);
		auto dsp = disp(pos[i], pos[j]);
		for (int d = 0; d < 3; d++) {
			f[i][d] += a * dsp[d] / r;
		}
	}

	return f;
}

std::vector<std::array<float,3>> shear_forces(const std::vector<std::array<float,3>> pos) {
	int N = pos.size();
	auto f = forces(pos);

	// Calculate accelerations.
	for (int i = 0; i < N; i++) {
		f[i][2] += cos(2*M_PI*pos[i][0]/L);
	}
	return f;
}
